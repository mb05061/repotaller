var clientesObtenidos;

function getCustomers() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest ();
  request .onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
    //  console .log(request.responseText);
    clientesObtenidos= request.responseText;
    procesarClientes();
    }
  }
  request .open("GET", url, true);
  request .send();
}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONProductosvalue)
var divTabla= document.getElementById("divTabla");
var tabla= document.createElement("table");
var tbody= document.createElement("tbody");

tabla.classList.add("table");
tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console .log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnaNombrecontacto = document.createElement("td");
    columnaNombrecontacto.innerText = JSONClientes.value[i].ContactName;
    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;
    var columnaBanderapais = document.createElement("td");
    //columnaBanderapais.innerText = JSONClientes.value[i].Country;
    var bandera = document.createElement("img");
    bandera.classList.add("flag");

    if (JSONClientes.value[i].Country == "UK") {
      bandera.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png";
    } else {
      bandera.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+JSONClientes.value[i].Country+".png";
    }


    nuevaFila.appendChild(columnaNombrecontacto);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBanderapais);
    columnaBanderapais.appendChild(bandera);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
